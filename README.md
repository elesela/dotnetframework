# .Net Framework Example

This is an example repo demonstrating how to create a Gitlab CI pipeline using Windows shared runners, to build and test a simple .Net Framework helloworld project with MSBuild and MSTest.

The code was taken from [MicrosoftDocs](https://github.com/MicrosoftDocs/visualstudio-docs/blob/master/docs/test/getting-started-with-unit-testing.md)
